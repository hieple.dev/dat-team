import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Keyboard,
    Image,
    Alert
  } from 'react-native';
import Loading from '../Loading/index';
import TimerMixin from 'react-timer-mixin';
import { insertNewAccountToServer } from '../NetworkAPI/index';
import i18n from '../../locales/i18n';

export default class RegisterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      configPassword: "",
      loading: false
    };
  }
  onSubmit = () => {
    if (this.state.password == ""||this.state.email == ""||this.state.configPassword == "") {
      Alert.alert(
        i18n.t('trans_ResgisterAlert.trans_RegisterFormAlert.alertTitle'),
        i18n.t('trans_ResgisterAlert.trans_RegisterFormAlert.emptyInput'),
        [{ text: "OK" }],
        { cancelable: false }
      );
      this.setState({ loading: false });
    } else if (this.state.password.length < 6||this.state.email <6||this.state.configPassword <6) {
      Alert.alert(
        i18n.t('trans_ResgisterAlert.trans_RegisterFormAlert.alertTitle'),
        i18n.t('trans_ResgisterAlert.trans_RegisterFormAlert.inputLength'),
        [{ text: "OK" }],
        { cancelable: false }
      );
      this.setState({ loading: false });
    } else {
      if (this.state.password!=this.state.configPassword){
        Alert.alert(
          i18n.t('trans_ResgisterAlert.trans_RegisterFormAlert.alertTitle'),
          i18n.t('trans_ResgisterAlert.trans_RegisterFormAlert.passwordConfirm'),
          [{ text: "OK" }],
          { cancelable: false }
        );
        this.setState({ loading: false });
      } else {
        this.setState({ loading: true });
        const newAccount = {
          email: this.state.email,
          password: this.state.password
      };    
      insertNewAccountToServer(newAccount).then((message) => {
        if (message=="Successfully") {
          TimerMixin.setTimeout(() => {
            this.setState({ loading: false });
            this.setState({
              email: "",
              password: "",
              configPassword: "",
            });
            Alert.alert(
              i18n.t('trans_ResgisterAlert.trans_RegisterSuccess.alertTitle'),
              i18n.t('trans_ResgisterAlert.trans_RegisterSuccess.alertContent'), +
              this.state.email,
              [
                {
                  text: i18n.t('trans_ResgisterAlert.trans_RegisterSuccess.alertText'),
                  onPress: () => this.props.navigation.navigate("Login")
                }
              ],
              { cancelable: false }
            );
            
          }, 500);
        } else {
          Alert.alert(
            i18n.t('trans_ResgisterAlert.trans_RegisterFailed.alertTitle'),
            i18n.t('trans_ResgisterAlert.trans_RegisterFailed.alertContent'),
            [
              {
                text:  i18n.t('trans_ResgisterAlert.trans_RegisterFailed.alertText'),
              }
            ],
            { cancelable: false }
          );
          this.setState({ loading: false });
        }
      }).catch((error)=>{
        alert(message);
      });      
      }
    }
  };
  onChangeText = (field, text) => {
    this.state[field] = text;
    this.forceUpdate();
  };
  render() {
    const Divider = (props) => {
        return <View {...props}>
          <View style={styles.line}></View>
        </View>
      }
    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View>
        <Image source={require('../../assets/img/background/background_register.png')} style={styles.backgroundImage} />
        <View style={styles.loginContainer}>
          <View style={styles.up}>
            <Image source={require('../../assets/icons/auth/avatar_header.png')} style={styles.logo}></Image>            
          </View>
          <View style={styles.down}>
            <View style={styles.textInputContainer}>
            <Image source={require('../../assets/icons/auth/user_icon.png')} style={{width: 25, height: 25, alignItems: 'center',justifyContent: 'center', marginTop: 8,}}></Image>
                <View style={styles.textInputView}>
                    <TextInput
                      style={styles.textInput}
                      textContentType='emailAddress'
                      keyboardType='email-address'
                      placeholder={i18n.t('trans_RegisterScreen.emailLabel')}
                      placeholderTextColor="rgba(1,1,1,0.8)"
                      value={this.state.email}
                      onChangeText={this.onChangeText.bind(this, 'email')} 
                    />
                </View>
            </View>
            <View style={styles.textInputContainer}>
            <Image source={require('../../assets/icons/auth/lock_icon_pw_1.png')} style={{width: 25, height: 25,  marginTop: 8,}}></Image>
                <View style={styles.textInputView}>
                    <TextInput
                      style={styles.textInput}
                      secureTextEntry = {true}
                      placeholder={i18n.t('trans_RegisterScreen.passwordLabel')}
                      placeholderTextColor="rgba(1,1,1,0.8)"
                      value={this.state.password}
                      onChangeText={this.onChangeText.bind(this, 'password')}
                    />
                </View>
            </View>
            <View style={styles.textInputContainer}>
            <Image source={require('../../assets/icons/auth/lock_icon_pw_2.png')} style={{width: 25, height: 25,  marginTop: 8,}}></Image>
                <View style={styles.textInputView}>
                  <TextInput
                    style={styles.textInput}
                    placeholder={i18n.t('trans_RegisterScreen.confirmPassword')}
                    placeholderTextColor="rgba(1,1,1,0.8)"
                    secureTextEntry={true}
                    value={this.state.configPassword}
                    onChangeText={this.onChangeText.bind(this, "configPassword")}
                  />
                </View>
            </View>
            <TouchableOpacity style={styles.loginButton} onPress={this.onSubmit}>
              <Text style={styles.loginButtonTitle}>{i18n.t('trans_RegisterScreen.registerBtn')}</Text>
            </TouchableOpacity> 
            <Divider style={styles.divider}></Divider>
            <View style={{flexDirection: 'row',marginTop: 10,}}>
            <Text style={styles.title1}>{i18n.t('trans_ForgotPasswordLink.forgotPassword')}</Text>
            <Text style={styles.title2}  onPress={() => this.props.navigation.goBack()}>{i18n.t('trans_RegisterScreen.backtoLoginBtn')}</Text>
            </View>
          </View>
        </View>
        {this.state.loading == true && <Loading />}
        </View>
      </TouchableWithoutFeedback>
        
   
    );
  }
}
const styles = StyleSheet.create({
    title1: {
        fontSize: 16,
        color: 'white',
        fontFamily: "Lobster-Regular",
    },
    title2: {
        marginLeft: 90,
        fontSize: 16,
        color: 'white',
        fontFamily: "Lobster-Regular",
    },
    backgroundImage: {
        height: '100%',
        width: '100%'
      },
    loginContainer: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
      },
      up: {
        flex: 5,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
      },
      down: {
        flex: 7,//70% of column
        marginTop: 20,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center'
      },
      title: {
        color: 'white',
        color: 'red',
        textAlign: 'center',
        width: 400,
        fontSize: 23
      },
      textInputContainer: {
        paddingHorizontal: 10,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: 'white',
        flexDirection: 'row',
        justifyContent: 'center',
        borderRadius: 6,
        marginBottom: 20,
       
      },
      textInput: {
        marginLeft: 10, 
        fontFamily: "Lobster-Regular",
      },
      textInputView: {
        marginLeft: 10,
        marginRight: -10,
        justifyContent: 'center',
        alignItems: 'flex-start',
        width: 300,
        height: 40,
        borderRadius: 5,
        backgroundColor: 'rgba(255,255,255,0.7)',
        
      },
      loginButton: {
        width: 320,
        height: 45,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#48dab7',

       
      },
      loginButtonTitle: {
        fontSize: 18,
        color: 'white',
        fontFamily: "Lobster-Regular",

      },
      facebookButton: {
        width: 300,
        height: 45,
        borderRadius: 6,
        justifyContent: 'center',
      },
      line: {
        height: 1.5,
        marginTop: 20,
        flex: 2,
        backgroundColor: 'white'
      },
      textOR: {
        flex: 1,
        textAlign: 'center'
      },
      divider: {
        flexDirection: 'row',
        height: 40,
        width: 298,
        justifyContent: 'center',
        alignItems: 'center'
      },
      logo:{
        marginTop: 40,
        width: 310,
        height: 135,
      }
})
