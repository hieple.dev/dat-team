import React, { Component } from 'react';
import { View, StyleSheet, Image, TextInput, Text, TouchableHighlight,AsyncStorage } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import i18n from '../../locales/i18n';
import { getCategoryFromServer, checkoutforProduct } from '../NetworkAPI/index';
export default class example2 extends Component {
    static navigationOptions = {
        headerLeft: (
            <Image
                source={require('../../assets/img/logo/logo_icon.png')}
                style={{ width: 120, height: 40, marginLeft: 10 }}
            />
        ),
        title: 'Checkout Product',
        headerStyle: {
            backgroundColor: '#3ee2bf',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: '400',
            marginLeft: 95,
            marginTop: 10,
            fontSize: 28,
            fontFamily: "Lobster-Regular",
        },
    };
    constructor(props) {
        super(props);
        this.state = {
            cartItems: [],
            idUser: '',
            note: '',
            phone: '',
            address: '',
            fullname: '',
        };
    }
    componentDidMount() {
        this._checkUserUsingApp();
      
    }
    _checkUserUsingApp = async () => {   
      try {     
           const usernameUsingApp = await AsyncStorage.getItem('idUserUsingApp'); 
           this.setState({
               idUser : usernameUsingApp
           })
          } catch (error) {   
        // Error retrieving data  
      } 
    };
    _checkoutProduct = () => {
        if (this.state.idUser==null&&this.state.fullname==null&& this.state.address==null&&this.state.phone==null&&this.state.note==null) {
            alert('Please enter your infomation');
        } else {
            
        }
        const checkoutCart = {
            user_id: this.state.idUser,
            orderDate: '2019-03-29 08:30:15',
            customerDelivery: this.state.fullname,
            deliveryAddress: this.state.address,
            phoneNumber: this.state.phone,
            note: this.state.note,
        }; 
        checkoutforProduct(checkoutCart).then((data) => {
            
            alert('The products is processing!!!');
          }).catch((error) => {
            alert('is not ok');
          })
    }
    componentWillMount() {
        AsyncStorage.getItem("CART", (err, res) => {
          if (!res) {
            this.setState({ cartItems: [] });
          }
          else {
            this.setState({ cartItems: JSON.parse(res) });
          }
        });
      }
    render() {
        return (
            <View style={{ flex: 1, alignItems: "center" }}>
                <Image style={styles.backgroundImg} source={require('../../assets/img/background/background_orderandcheckout.png')}></Image>
                <View style={styles.contentWrap}>
                    <View style={styles.formWrap}>
                        <View style={styles.titles}>
                            <Text style={styles.title}>Full name</Text>
                            <TextInput style={styles.myinput}
                               
                                placeholderTextColor="rgba(1,1,1,0.8)"
                                onChangeText={(text) => this.setState({fullname: text})}
                            ></TextInput>
                        </View>
                        <View style={styles.titles}>
                            <Text style={styles.title}>Phone</Text>
                            <TextInput style={styles.myinput}
                               
                                placeholderTextColor="rgba(1,1,1,0.8)"
                                onChangeText={(text) => this.setState({phone:text})}
                            ></TextInput>
                        </View>
                        <View style={styles.titles}>
                            <Text style={styles.title}>Address</Text>
                            <TextInput style={styles.myinput}
                               
                                placeholderTextColor="rgba(1,1,1,0.8)"
                                onChangeText={(text) => this.setState({address:text})}
                            ></TextInput>
                        </View>
                        <View style={styles.titles}>
                            <Text style={styles.title}>Note</Text>
                            <TextInput style={styles.myinput}
                              
                                placeholderTextColor="rgba(1,1,1,0.8)"
                                onChangeText={(text) => this.setState({note:text})}
                                multiline={true}
                            ></TextInput>
                        </View>
                    </View>
                   
                    <View style={styles.buttonCart}>
                        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#ff8100', '#ff4300', '#ff1100']} style={styles.linearGradient}>
                            <TouchableHighlight
                          onPress={() => 
                            this._checkoutProduct()
                            }
                            >
                                <Text style={[styles.btnText]}>{i18n.t('trans_CartDetailScreen.checkoutBtn')}</Text>
                            </TouchableHighlight>
                        </LinearGradient>
                    </View>
                   
                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    backgroundImg: {
        width: '100%',
        height: '100%',
        opacity: 0.2,
    },

    btnText: {
        color:'#fff',
        textAlign:'center',
        fontFamily: 'Lobster-Regular',
        fontSize: 20,
    },

    linearGradient: {
        flex: 1,
        paddingLeft: 60,
        paddingRight: 60,
        paddingTop: 7,
        paddingBottom: 7,
        borderRadius: 30,
    },

    contentWrap: {
        marginTop:20,
        flex: 1,
        position: "absolute",
       
    },

    buttonCart: {
        alignSelf: 'center',
        position: 'absolute',
        bottom: 2,
        marginBottom: -20,
    },

    formWrap: {
        width: 400,
        height: 480,
        borderRadius: 15,
        backgroundColor: 'transparent',
        borderWidth: 3,
        borderColor: '#000',
        marginTop: 0,
    },

    titles: {
        flexDirection: 'row',
        height: 50,
        borderWidth: 1,
        borderColor: "#000",
        borderRadius: 10,
        marginTop: 10,
        marginLeft: 5,
        marginRight: 5,
    },

    title: {
        flex: 3,
        fontFamily: "Lobster-Regular",
        fontSize: 18,
        color: "#000",
        marginLeft: 10,
        marginTop: 10,
    },

    myinput: {
        flex: 7,
        fontFamily: "Lobster-Regular",
        fontSize: 18,
        color: "#000",
        marginLeft: 10,
    },

    mytext: {
        fontFamily: "Lobster-Regular",
        fontSize: 16,
        color: "#000",
        marginLeft: 10,
    },

    btnwrap: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },

    confirmbtn: {
        position: "absolute",
        width: 350,
        height: 50,
        backgroundColor: "#FF6600",
        borderRadius: 100,
        marginTop: 5,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    confirmtxt: {
        fontFamily: "Lobster-Regular",
        fontSize: 25,
        color: "#fff",
        textAlign: "center"
    }

})  
