const ipAPIforApp = 'http://datapplication.tryfcomet.com'
const apiGetUserInfo = ipAPIforApp + '/api/checkLogin'
const apiUpdateUserInfo = ipAPIforApp + '/api/editUser/' 
async function getUserInfoFromServer(params) {
    try {
        let response = await fetch(apiGetUserInfo,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        });
        let responseJson = await response.json();
        console.log(responseJson);
        return responseJson.userinfo;
    } catch (error) {
        console.error(`Error is: ${error}`);
    }
}
async function updateUserProfile(params, id) {
    try {
        let response = await fetch(apiUpdateUserInfo+id,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        });
        let responseJson = await response.json();
        console.log(responseJson);
        return responseJson.message;
    } catch (error) {
        console.error(`Error is: ${error}`);
    }
}

export {getUserInfoFromServer};
export {updateUserProfile}


