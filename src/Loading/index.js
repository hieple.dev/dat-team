import React from 'react';
import { View, Modal, ActivityIndicator, Platform } from 'react-native';

//VARIABLES


//COMPONENTS


//STYLESHEET
import styleSheet from './styles';

export default class Loading extends React.Component {
  constructor(props) {
    super(props);
  }
  render = () => {
    return (
      <Modal visible={true} transparent={true} onRequestClose={() => { }}>
        <View style={[styleSheet.common.container, { position: 'relative', backgroundColor: 'black', opacity: 0.7 }]}>
          <ActivityIndicator size={Platform.OS == 'ios' ? 'small' : 25} style={{ position: 'absolute', alignSelf: 'center' }} />
        </View>
      </Modal>
    );
  }
}