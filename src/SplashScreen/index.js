import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  Animated,
  Image,
  Dimensions,
  AsyncStorage
} from 'react-native'
import TimerMixin from 'react-timer-mixin';
var {height, width} = Dimensions.get('window')
const LOGO_IMAGE = require("../../assets/img/logo/logo_app.png");
const WINDOW_WIDTH = Dimensions.get("window").width;
import i18n from '../../locales/i18n';

export default class Splash extends Component {
    state = {
        logoOpacity: new Animated.Value(0),
        titleMarginTop: new Animated.Value(height / 2)
    }
    async componentDidMount() {
        Animated.sequence([
            Animated.timing(this.state.logoOpacity,{
                toValue: 1,                  
                duration: 2000,              
            }),
            Animated.timing(this.state.titleMarginTop, {
                toValue: 10,
                duration: 1000,
            })
        ]).start(() => {
              TimerMixin.setTimeout(() => {
              this._checkUserUsingApp();
              }, 2000);
        })
    }
    _checkUserUsingApp = async () => {   
      try {     
          const value = await AsyncStorage.getItem('emailUsingApp'); 
          if (value !== null) {       
              this.props.navigation.navigate('Main');
              } else {
              this.props.navigation.navigate("Login");
            }   
          } catch (error) {   
        // Error retrieving data  
      } 
    };
  
    render() {
        return <View style={styles.container}>
        <Animated.View style={styles.logoContainer}>
          <Animated.Image style={{...styles.logo,opacity: this.state.logoOpacity}} source={LOGO_IMAGE} />

            <View style={{height: 20}}></View>
            <Animated.Text style={{...styles.title, marginTop:this.state.titleMarginTop}}>
              {i18n.t('trans_SplashScreen.slogan')}
            </Animated.Text>
        </Animated.View>
      </View>
    }
}
const styles = StyleSheet.create({
    container: {
      backgroundColor: "rgb(32, 53, 70)",
      flex: 1,
      alignItems: "center",
      justifyContent: "center"
    },
    title: {
      fontFamily: "Lobster-Regular",
      fontSize: 20,
      color: "white"
    },
    logoContainer: {
      alignItems: "center",
      justifyContent: "center",
      flex: 1,
    },
    logo: {
        width: 180,
        height: 150
    },
  });