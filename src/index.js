import React from 'react';
import { AsyncStorage, Text, View, Image } from 'react-native';
import { createStackNavigator, createBottomTabNavigator, createAppContainer } from 'react-navigation';
import ListProducts from './ListProducts/index';
import HomeScreen from './CategoryScreen/index'
import HistoryScreen from './HistoryBuyProduct/index'
import CartScreen from './CartScreen/index';
import OrderCheckout from './OrderandCheckoutScreen/index';
import ViewProductDetailScreen from './ViewProductDetail/index';
import ViewImageProductDetailScreen from './ViewProductDetail/ImageofProducts';
import LoginScreen from './LoginScreen/index';
import RegisterScreen from './RegisterScreen/index';
import SplashScreen from './SplashScreen/index';
import SettingScreen from './SettingScreen/index';
import i18n from '../locales/i18n'


const HomeStack = createStackNavigator({
  Home: { screen: HomeScreen },
  ListProductfromCategory: { screen: ListProducts },
  ViewProductDetail: { screen: ViewProductDetailScreen },
  ViewImageProducts: { screen: ViewImageProductDetailScreen },
});

const HistoryStack = createStackNavigator({
  History: { screen: HistoryScreen },
});

const CartStack = createStackNavigator({
  Cart: { screen: CartScreen },
  OrderandCheckout: { screen: OrderCheckout }
});

const ProfileStack = createStackNavigator({
  Settings: { screen: SettingScreen },
});
// _getQuantityCart=()=>{
//   AsyncStorage.getItem("CART", (err, res) => {
//     if (!res){
//       this.setState({cartItems: []});
//     }
//     else{
//        this.setState({cartItems: JSON.parse(res)});
//        alert(this.state.cartItems.length)
//   }
//   });
// }

const TabRoutes = createBottomTabNavigator({
  Home: {
    screen: HomeStack,
    navigationOptions: {
      tabBarLabel: i18n.t('trans_HeaderSection.headerTitleCategory'),
      tabBarIcon: (
        <Image
          source={require('../assets/icons/tab/icons_list.png')}
          style={{ width: 40, height: 40, marginRight: 10, opacity: 10, justifyContent: 'center', alignItems: 'center' }} />
      )
    }
  },
  Histories: {
    screen: HistoryStack,
    navigationOptions: {
      tabBarLabel: i18n.t('trans_HeaderSection.headerTitleHistory'),
      tabBarIcon: (
        <Image
          source={require('../assets/icons/tab/icons_ls.png')}
          style={{ width: 40, height: 40, marginRight: 10, opacity: 10, justifyContent: 'center', alignItems: 'center' }} />
      )
    }
  },
  Carts: {
    screen: CartStack,
    navigationOptions: {
      tabBarLabel: i18n.t('trans_HeaderSection.headerTitleCart'),
      tabBarIcon: (
        <Image
          source={require('../assets/icons/tab/icons_cart.png')}
          style={{ width: 40, height: 40, marginRight: 10, opacity: 10, justifyContent: 'center', alignItems: 'center' }} />
      )
    }
  },
  Profile: {
    screen: ProfileStack,
    navigationOptions: {
      tabBarLabel: 'Profile',
      tabBarIcon: (
        <Image
          source={require('../assets/icons/tab/icons_user.png')}
          style={{ width: 40, height: 40, marginRight: 10, opacity: 10, justifyContent: 'center', alignItems: 'center' }} />
      )
    }
  },
},
  {
    tabBarOptions: {
      initialRouteName: 'Home',
      activeTintColor: 'black',
      inactiveTintColor: 'gray',
      labelStyle: {
        fontSize: 14,
        marginLeft: -10,
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: "Lobster-Regular",
      },
      style: {
        backgroundColor: '#3ee2bf',
        height: 70,
      },
    },
  }
)

const RootStack = createStackNavigator({
  Splash: {
    screen: SplashScreen,
    navigationOptions: () => ({
      header: null
    })
  },
  Login: {
    screen: LoginScreen,
    navigationOptions: () => ({
      header: null
    })
  },
  Register: {
    screen: RegisterScreen,
    navigationOptions: () => ({
      header: null
    })
  },
  Main: {
    screen: TabRoutes
  }
},
  {
    defaultNavigationOptions: {
      gesturesEnabled: false,
      header: null
    },
  });

export default createAppContainer(RootStack);
