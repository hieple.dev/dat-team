import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Alert,
  TouchableOpacity,
  Text,
  AsyncStorage,
  BackHandler,
  Image
}
  from 'react-native';
import { FlatGrid } from 'react-native-super-grid';
import { getCategoryFromServer, loadDatafromUser } from '../NetworkAPI/index';
import i18n from '../../locales/i18n';
import Loading from '../Loading/index';
export default class CategoryScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      categoryFromServer: [],
      userUsingApp: '',
      passwordUsingApp: '',
      infoofUser: [],
    };
  }
 
  handleBackPress = () => {
    Alert.alert (
      'Exit App',
      'Do you want to exit?',
      [
        {
          text: 'No',
          style: 'cancel',
        },
        {text: 'Yes', onPress: () => BackHandler.exitApp ()},
      ],
      {cancelable: false}
    );
    return true;
  };
  componentWillUnmount() {
    BackAndroid.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentDidMount() {
      this.refreshDatafromServer();
      this._checkUserUsingApp();
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  _checkUserUsingApp = async () => {   
    try {     
         const usernameUsingApp = await AsyncStorage.getItem('usernameUsingApp'); 
         Alert.alert(
          'Welcome to',
         'Welcome to '+usernameUsingApp+' !!! Hope you happy with the application',
          [{ text: "OK" }],
          { cancelable: false }
        );
        } catch (error) {   
      // Error retrieving data  
    } 
  };
  refreshDatafromServer = () => {
    getCategoryFromServer().then((data) => {
      this.setState({
        loading: false,
        categoryFromServer: data
      });
    }).catch((error) => {
      this.setState({ categoryFromServer: [] })
    })
  }
  static navigationOptions = {
    headerLeft: (
      <Image
        source={require('../../assets/img/logo/logo_icon.png')}
        style={{ width: 125, height: 45, marginLeft: 10 }}
      />
    ),
    title: i18n.t('trans_HeaderSection.headerTitleCategory'),
    headerStyle: {
      backgroundColor: '#3ee2bf',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: '400',
      marginLeft: 95,
      marginTop: 10,
      fontSize: 28,
      fontFamily: "Lobster-Regular",
    },
  };
  render() {
    return (
      <View>
        <Image source={require('../../assets/img/background/background_wcyan_home.png')} style={styles.backgroundImage} />
        <View style={styles.loginContainer}>
          <FlatGrid
            itemDimension={240}
            items={this.state.categoryFromServer}
            style={styles.gridView}
            onClick={() => {
              Alert.alert('You tapped the button!');
            }}
            renderItem={({ item, index }) => {
              return <View style={[styles.itemContainer, { backgroundColor: 'white' }]}>
                <View style={{ backgroundColor: '#c7ecee', flex: 1, borderWidth: 2, borderColor: '#e84118', borderRadius: 10, flexDirection: 'row' }}>
                  <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
                    onPress={() => this.props.navigation.navigate('ListProductfromCategory', { itemId: item.id, })}>
                    <View>
                      <Image source={{ uri: item.categoryImage }} style={{ width: 185, height: 105, borderRadius: 10, marginLeft: 8 }} ></Image>
                    </View>
                  </TouchableOpacity>
                  <View style={{ flex: 1, justifyContent: 'center', marginTop: 10, marginBottom: 10, marginRight: 10, alignItems: 'center', marginLeft: 8, borderWidth: 2, borderColor: '#44bd32', borderRadius: 10, }}>
                    <Text style={styles.itemName}>{item.name}</Text>
                    {/* <Text style={styles.itemQuan}>Số lượng: <Text style={styles.itemCode}>{this.state.categoryFromServer[0].id}</Text></Text> */}
                  </View>

                </View>
              </View>
            }}
          />

        </View>
        {this.state.loading == true && <Loading />}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  backgroundImage: {
    height: '100%',
    width: '100%'
  },
  loginContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    flex: 1,
  },
  gridView: {
    flex: 1,
  },
  itemContainer: {
    flexDirection: 'row',
    borderWidth: 5,
    borderColor: '#0097e6',
    borderRadius: 10,
    padding: 10,
    height: 150,
  },
  itemQuan: {
    marginLeft: 10,
    fontSize: 20,
    color: 'blue',
    fontFamily: "Lobster-Regular",

  },
  itemName: {
    marginLeft: 10,
    fontSize: 25,
    color: 'black',
    fontFamily: "Lobster-Regular",

  },
  itemCode: {
    marginLeft: 10,
    fontSize: 20,
    color: 'red',
    fontFamily: "Lobster-Regular",
  },
});