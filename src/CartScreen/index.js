import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, AsyncStorage, FlatList, TouchableHighlight } from 'react-native';
import { FlatGrid } from 'react-native-super-grid';
import LinearGradient from 'react-native-linear-gradient';
import i18n from '../../locales/i18n';
import Icon from 'react-native-vector-icons/Ionicons';
import {resetList} from '../NetworkAPI/resetFunctions';

  class ListItem extends React.Component {
    render() {
      const { item } = this.props;
      return (

        <View style={styles.itemContainer}>
          <View style={{ flex: 3, }}>
            <Image source={{ uri: item.picture }} style={{ width: 80, height: 80, borderRadius: 5, }} />
          </View>
          <View style={{ flex: 7, }}>
            <Text style={styles.productName}>{item.productName} (  {item.quantity} kg ) </Text>
            <Text style={styles.productInfor}><Text>{i18n.t('trans_CartScreen.total')}:  </Text>
              <Text style={styles.productPrice}>{item.totalPrice}.000</Text><Text> VNĐ</Text></Text>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.productInfor}>{i18n.t('trans_CartScreen.quantity')}  </Text>
              <TouchableHighlight
                style={styles.btn}
                onPress={this.props.onSubtract}
              >
                <Image source={require('../../assets/icons/cart/subtract.png')} style={{ width: 20, height: 20, }} />
              </TouchableHighlight>
              <Text style={styles.productInfor}>   {item.quantity}   </Text>
              <TouchableHighlight
                style={styles.btn}
                onPress={this.props.onAdd}
              >
                <Image source={require('../../assets/icons/cart/plus.png')} style={{ width: 20, height: 20, }} />
              </TouchableHighlight>
            </View>
          </View>
        </View>
      )
    }
  }
export default class cartScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cartItems: [],
    };
  }
  static navigationOptions = {
    headerLeft: (
      <Image
        source={require('../../assets/img/logo/logo_icon.png')}
        style={{ width: 120, height: 40, marginLeft: 10 }}
      />
    ),
    title: i18n.t('trans_HeaderSection.headerTitleCart'),
    headerStyle: {
      backgroundColor: '#3ee2bf',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: '400',
      marginLeft: 95,
      marginTop: 10,
      fontSize: 28,
      fontFamily: "Lobster-Regular",
    },
  };
  onSubtract = (item, index) => {
    const cartItems = [...this.state.cartItems];
    if (cartItems[index].quantity == 1) {
      cartItems[index].quantity == 1;
      cartItems[index].totalPrice = cartItems[index].quantity * cartItems[index].price;
      this.setState({ cartItems });
    } else {
      cartItems[index].quantity -= 1;
      cartItems[index].totalPrice = cartItems[index].quantity * cartItems[index].price;
      this.setState({ cartItems });
    }
  }
  onAdd = (item, index) => {
    const cartItems = [...this.state.cartItems];
    if (cartItems[index].quantity == 20) {
      cartItems[index].quantity == 20;
      cartItems[index].totalPrice = cartItems[index].quantity * cartItems[index].price;
      this.setState({ cartItems });
    } else {
      cartItems[index].quantity += 1;
      cartItems[index].totalPrice = cartItems[index].quantity * cartItems[index].price;
      this.setState({ cartItems });
    }
  }
  componentWillMount() {
    AsyncStorage.getItem("CART", (err, res) => {
      if (!res) {
        this.setState({ cartItems: [] });
      }
      else {
        this.setState({ cartItems: JSON.parse(res) });
      }
    });
  }
  _resetList() {
    AsyncStorage.getItem("CART", (err, res) => {
      if (!res) {
        this.setState({ cartItems: [] });
      }
      else {
        this.setState({ cartItems: JSON.parse(res) });
      }
    });
  }
  async resetCart() {
    try {
      await AsyncStorage.removeItem('CART');
      const value = await AsyncStorage.getItem('CART');
      if (value != null) {

      } else {

        this._resetList()
      }
    } catch (error) {
      console.log("Error resetting data" + error);
    }
  }
 
  showDataCart() {
    if (this.state.cartItems.length == 0) {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
          <Text style={{
            fontSize: 20,
            color: 'black',
            fontFamily: "Lobster-Regular",
          }}
          >Your cart is empty...</Text>
        </View>
      )

    } else {
      return (
        <FlatList
          itemDimension={200}
          data={this.state.cartItems}
          style={styles.gridView}

          renderItem={({ item, index }) =>
            <ListItem
              item={item}
              onSubtract={() => this.onSubtract(item, index)}
              onAdd={() => this.onAdd(item, index)}
            />
          }
          keyExtractor={item => item.id}
        />
      )
    }
  }
  render() {
    const { item } = this.props;
    const { cartItems } = this.state;
    let totalPro = 0;
    let totalFree = '10%';
    cartItems.forEach(item => {
      totalPro += item.totalPrice;
    })
    let totalOrder = totalPro + ((totalPro*10)/100);

    return (

      <View style={{ flex: 1, flexDirection: 'column' }}>

        <View style={{ flex: 1 }}>

          <View style={styles.listProduct}>
            {this.showDataCart()}
          </View>
          <View style={{ width: '100%', height: 40, justifyContent: 'center', flexDirection: 'row' }}>
            <TouchableHighlight onPress={() => this._resetList()} >
              <Text style={{ fontFamily: 'Lobster-Regular', color: 'blue', fontSize: 20, marginTop: 10 }}>
              {i18n.t('trans_CartScreen.refreshCartBtn')}
                  </Text>
            </TouchableHighlight>
            <View style={{ width: 80 }}>
            </View>
            <TouchableHighlight onPress={() => this.resetCart()} >
              <Text style={{ fontFamily: 'Lobster-Regular', color: 'red', fontSize: 20, marginTop: 10 }}>
              {i18n.t('trans_CartScreen.emptyCartBtn')}
                  </Text>
            </TouchableHighlight>
          </View>
          <View style={styles.calculatingSession}>
            <View style={{ flexDirection: 'row', marginTop: 20 }}>
              <Text style={styles.titleCost}>{i18n.t('trans_CartScreen.total')}</Text>
              <Text style={styles.colon}>:</Text>
              <Text style={styles.cost}>{totalPro}.000</Text>
              <Text style={styles.currency}>VNĐ</Text>
            </View>
            <View style={styles.line} />
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.titleCost}>{i18n.t('trans_CartScreen.shippingFee')}</Text>
              <Text style={styles.colon}>:</Text>
              <Text style={styles.cost}>{totalFree}</Text>
              <Text style={styles.currency}>VNĐ</Text>
            </View>
            <View style={styles.line} />
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.titleCost}>{i18n.t('trans_CartScreen.totalCost')}</Text>
              <Text style={styles.colon}>:</Text>
              <Text style={styles.cost}>{totalOrder}.000</Text>
              <Text style={styles.currency}>VNĐ</Text>
            </View>
          </View>
        </View>
        <View style={styles.buttonCart}>
          <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#ff8100', '#ff4300', '#ff1100']} style={styles.linearGradient}>
            <TouchableHighlight
              // onPress={() => alert(this.state.cartItems.map((data)=>{
              //   return(
              //     data.productName
              //   )
              // }))}
              // onPress={() => alert(JSON.stringify(this.state.cartItems))}
              onPress={() => this.props.navigation.navigate('OrderandCheckout')}
            >
              <Text style={[styles.btnText]}>{i18n.t('trans_CartScreen.orderBtn')}</Text>
            </TouchableHighlight>
          </LinearGradient>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    height: '100%',
    width: '100%'
  },
  listProduct: {
    flex: 7,
    borderColor: '#000000',
    borderTopWidth: 3,
    borderBottomWidth: 3,
    borderLeftWidth: 4,
    borderRightWidth: 4,
    borderRadius: 10,
    marginTop: 10,
    marginRight: 5,
    marginLeft: 5,
  },
  gridView: {
    flex: 1,

  },
  itemContainer: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    borderColor: '#3ee2bf',
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderLeftWidth: 3,
    borderRightWidth: 3,
    borderRadius: 5,
    padding: 7,
    height: 100,
  },
  productName: {
    fontFamily: 'Lobster-Regular',
    color: 'black',
    fontSize: 20,
  },
  productInfor: {
    fontFamily: 'Lobster-Regular',
    color: 'black',
    fontSize: 16,
  },
  productPrice: {
    color: 'red',
  },


  calculatingSession: {
    flex: 3,
    borderColor: '#000000',
    borderTopWidth: 3,
    borderBottomWidth: 3,
    borderLeftWidth: 4,
    borderRightWidth: 4,
    borderRadius: 10,
    marginTop: 5,
    marginRight: 5,
    marginLeft: 5,
    marginBottom: 15,
    paddingLeft: 30,
    paddingRight: 30,
  },
  line: {
    marginTop: 2,
    borderBottomColor: 'black',
    borderBottomWidth: 2,
  },
  titleCost: {
    fontFamily: 'Lobster-Regular',
    color: 'black',
    fontSize: 20,
  },
  colon: {
    fontFamily: 'Lobster-Regular',
    color: 'black',
    fontSize: 20,
    position: 'absolute',
    left: 150,
  },
  cost: {
    fontFamily: 'Lobster-Regular',
    color: 'red',
    fontSize: 20,
    position: 'absolute',
    right: 50,
  },
  currency: {
    fontFamily: 'Lobster-Regular',
    color: 'black',
    fontSize: 20,
    position: 'absolute',
    right: 0,
  },
  buttonCart: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: 2,
  },
  btnText: {
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'Lobster-Regular',
    fontSize: 20,
  },

  btnCart: {
    width: 200,
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#c96c6e',
    borderRadius: 30,
    borderWidth: 1,
    borderColor: '#fff',
    justifyContent: "flex-end",
    marginLeft: 7.5,
    marginRight: 15,
  },

  linearGradient: {
    flex: 1,
    paddingLeft: 60,
    paddingRight: 60,
    paddingTop: 7,
    paddingBottom: 7,
    borderRadius: 30,
  },
});
