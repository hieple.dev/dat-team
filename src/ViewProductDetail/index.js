import React, { Component } from 'react';
import {
    View,
    Image,
    StyleSheet,
    TextInput,
    Text,
    TouchableOpacity,
    TouchableHighlight,
    Alert,
    AsyncStorage,
    ScrollView,
    FlatList
} from 'react-native';
import i18n from '../../locales/i18n';
import LinearGradient from 'react-native-linear-gradient';
import Modal from "react-native-modal";
import { getCommentProductDetail, commentforProduct } from '../NetworkAPI/index';
import Loading from '../Loading/index';
import Icon from 'react-native-vector-icons/Ionicons';

export default class ViewProductDetail extends Component {
    static navigationOptions = {
        title: i18n.t('trans_HeaderSection.headerTitleViewProductDetail'),
        headerStyle: {
            backgroundColor: '#3ee2bf',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: '400',
            marginLeft: 70,
            marginTop: 10,
            fontSize: 28,
            fontFamily: "Lobster-Regular",
        },
        // headerRight: (
        //     <View style={{ flexDirection: 'row' }}>
        //         <Image
        //             source={require('../../assets/icons/tab/icons_cart.png')}
        //             style={{ width: 30, height: 30, marginRight: 20, opacity: 10, justifyContent: 'center', alignItems: 'center' }} />

        //         <View style={{
        //             width: 20,
        //             height: 20,
        //             backgroundColor: "#000",
        //             borderRadius: 100, marginTop: 15, marginLeft: 20, justifyContent: 'center', alignItems: 'center', position: "absolute",
        //         }}>
        //             <Text style={{ color: 'red', fontFamily: 'Lobster-Regular', fontSize: 12, marginTop: 20, position: "absolute", }}>0</Text>

        //         </View>
        //     </View>
        // ),
    };
    constructor(props) {
        super(props);
        this.maxLength = 100;
        this.state = {
            loading: true,
            priceofProducts: '',
            listCommentofProduct: [],
            showButton: this.componentButtonAddtoCart(),
            isComment: this.componentClose(),
            visibleModal: null,
            textLength: 100,
            product: {},
            quantity: 111,
            selectedColor: '1blacksa',
            selectedSize: 3011,
            cartItems: [],
            quantity: 1,
            usernameComment: '',
            avatarUserComment: '',
            idUserComment: '',
            emailUserComment: '',
            commentContent: '',
        }
    }
    onSubtract = () => {
        if (this.state.quantity == 1) {
            this.setState({ quantity: 1 });
        } else {
            this.setState({ quantity: this.state.quantity - 1 });
        }
    }
    onAdd = () => {
        if (this.state.quantity == 20) {
            this.setState({ quantity: 20 });
        } else {
            this.setState({ quantity: this.state.quantity + 1 });
        }
    }
    _addToCard = async () => {
        try {
            var product = this.state.product;
            const { navigation } = this.props;
            const priceProductAdd = navigation.getParam('priceProduct', 'NO-ID');
            let totalPR = this.state.quantity * priceProductAdd;
            product['productName'] = navigation.getParam('nameProduct', 'NO-ID');
            product['totalPrice'] = totalPR;
            product['price'] = navigation.getParam('priceProduct', 'NO-ID');
            product['netWeight'] = this.state.quantity;
            product['quantity'] = this.state.quantity;
            product['picture'] = navigation.getParam('linkImageProduct', 'NO-ID');
            AsyncStorage.getItem("CART", (err, res) => {
                if (!res) AsyncStorage.setItem("CART", JSON.stringify([product]));
                else {
                    var items = JSON.parse(res);
                    items.push(product);
                    
                    AsyncStorage.setItem("CART", JSON.stringify(items));
                    

                }
                alert('Add to cart succesfully!!!');
            });
        } catch (error) {
            // Error saving data
        }
    };
    _checkUserUsingApp = async () => {   
        try {     
            const emailUsingApp = await AsyncStorage.getItem('emailUsingApp'); 
            const idUserUsingApp = await AsyncStorage.getItem('idUserUsingApp'); 
            const usernameUsingApp = await AsyncStorage.getItem('usernameUsingApp'); 
            const userAvatarUsingApp = await AsyncStorage.getItem('avataImageUsingApp'); 
            this.setState({
                usernameComment: usernameUsingApp,
                avatarUserComment: userAvatarUsingApp,
                idUserComment: idUserUsingApp,
                emailUserComment: emailUsingApp
            })
            } catch (error) {   
          // Error retrieving data  
        } 
      };
    componentDidMount() {
        this.refreshDatafromServer();
        this._checkUserUsingApp();
    }
    refreshDatafromServer = () => {
        const { navigation } = this.props;
        const itemId = navigation.getParam('itemIdProduct', 'NO-ID');
        getCommentProductDetail(itemId).then((data) => {
            this.setState({
                loading: false,
                listCommentofProduct: data
            })
        }).catch((error) => {
            this.setState({
                loading: false,
                listCommentofProduct: []
            })
        })
    }
    onChangeText(text) {
        this.setState({
            textLength: this.maxLength - text.length,
            commentContent: text
        });
        if (text.length > 0) {
            this.setState({
                isComment: this.componentComment()
            })
        } else {
            this.setState({
                isComment: this.componentClose()
            })
        }

    }
    showAlert(){
        const { navigation } = this.props;
        const itemIdPro = navigation.getParam('itemIdProduct', 'NO-ID');
        const commentPro = {
            
            email: this.state.emailUserComment,
            product_id: itemIdPro,
            description: this.state.commentContent,
            user_id : this.state.idUserComment
        }; 
        commentforProduct(commentPro).then((data) => {
                      this.refreshDatafromServer();
                    }).catch((error) => {
                      
                    })
        this.setState({ visibleModal: null})
    }
    componentComment() {
        return (
            <Icon name="md-send" size={35} color="black" onPress={()=>
               
              this.showAlert()
                
              
            } />
        
        );
    }
    
    componentClose() {
        return (
            <Icon name="md-close-circle-outline" size={35}   />
        );
       
    }
    componentButtonComment() {
        return (
            <View style={styles.buttonCart}>
                <TouchableHighlight
                    onPress={() =>
                        this.setState({ visibleModal: 1 })}>
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#ff8100', '#ff4300', '#ff1100']} style={styles.linearGradient}>

                        <Text style={styles.btnText}>{i18n.t('trans_CommentScreen.addCmtBtn')}</Text>

                    </LinearGradient>
                </TouchableHighlight>
            </View>
        );
    }
    componentButtonAddtoCart() {
        return (
            <View style={styles.buttonCart}>
                <TouchableOpacity
                    onPress={() => {
                        this._addToCard();
                    }}
                >
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#ff8100', '#ff4300', '#ff1100']} style={styles.linearGradient}>

                        <Text style={styles.btnText}>{i18n.t('trans_ProductDetailScreen.addtoCartBtn')}</Text>

                    </LinearGradient>
                </TouchableOpacity>
            </View>
        );
    }
    renderButton = (onPress) => (
        <TouchableOpacity style={{ right: -20 }} onPress={onPress}>
            <View style={styles.button}>
                {this.state.isComment}
            </View>
        </TouchableOpacity>
    );

    renderModalContent = () => (
        <View style={styles.modalContent}>
            <View style={{ flex: 1, }}>
                <View style={{ left: 0, flexDirection: "row" }}>
                    <View style={{ left: 0 }}>
                        <Image style={{ borderRadius: 50, width: 60, height: 60, borderWidth: 1, }} source={{ uri: this.state.avatarUserComment }}></Image>
                        <Text style={{ fontFamily: 'Lobster-Regular', color: 'blue' }}>{this.state.usernameComment}</Text>
                    </View>
                    <View style={{ justifyContent: "center" }}>
                        <TextInput onChangeText={this.onChangeText.bind(this)} placeholder='Enter your comment' placeholderTextColor='black' style={{ fontFamily: 'Lobster-Regular', marginLeft: 8, borderRadius: 5, width: 200, height: 50, borderWidth: 1, }}>
                        </TextInput>
                    </View>
                </View>
            </View>
            {this.renderButton(() => this.setState({ visibleModal: null }))}
        </View>
    );
    render() {
        const { navigation } = this.props;
        const nameProduct = navigation.getParam('nameProduct', 'NO-ID');
        const desProduct = navigation.getParam('desProduct', 'NO-ID');
        const pricePr = navigation.getParam('priceProduct', 'NO-ID');
        const itemIdPro = navigation.getParam('itemIdProduct', 'NO-ID');
        const linkImageThumnail = navigation.getParam('linkImageProduct', 'NO-ID');
        var quant = this.state.quantity;
        var total = 0;
        total = quant * pricePr;
        return (
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <Image style={styles.bgImg} source={{ uri: linkImageThumnail }}></Image>
                <View style={styles.contentwrap}>
                    <View style={{ flex: 1, flexDirection: 'row', marginTop: -10, height: 200, borderRadius: 5, justifyContent: 'center' }}>
                        {/* <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', padding: 10 }}>
                            <Image style={{width: 120, height: 160, borderRadius: 10, borderWidth: 5, borderColor: '#fff', marginRight: 120}} source={require('../../assets/img/fruit.jpg')}></Image>                        
                        </View> */}
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ViewImageProducts', { IdImage: itemIdPro, })}>
                            {/* onPress={() => this.props.navigation.navigate('ListProductfromCategory',{itemId: item.id,})}> */}
                            <View style={{ flex: 1, position: 'relative', justifyContent: 'center', alignItems: 'center', padding: 10 }}>
                                <Image style={{ width: 250, height: 200, borderRadius: 10, borderWidth: 5, borderColor: '#fff' }} source={{ uri: linkImageThumnail }}></Image>
                            </View>
                        </TouchableOpacity>
                        {/* <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', padding: 10 }}>
                            <Image style={{width: 120, height: 160, borderRadius: 10, borderWidth: 5, borderColor: '#fff', marginLeft: 120}} source={require('../../assets/img/fruit.jpg')}></Image>                        
                        </View> */}
                    </View>
                    <ScrollView horizontal={true} pagingEnabled={true}
                        onScroll={(event) => {
                            let logDate = `Scrolled to x = ${event.nativeEvent.contentOffset.x}, y = ${event.nativeEvent.contentOffset.y}`
                            let xScr = event.nativeEvent.contentOffset.x
                            let yScr = event.nativeEvent.contentOffset.y
                            console.log(logDate)
                            if (xScr > 368) {
                                this.setState({
                                    showButton: this.componentButtonComment()
                                })
                            }
                            if (xScr <= 368) {
                                this.setState({
                                    showButton: this.componentButtonAddtoCart()
                                })
                            }
                        }}
                        scrollEventThrottle={0.00001}>
                        <View style={styles.proInfoWrap}>
                            <View style={styles.proInfo}>
                                <Text style={styles.proTitle}>{nameProduct}</Text>
                                <Text style={styles.proPrice}>{i18n.t('trans_ProductDetailScreen.proPrice')}: <Text style={{ color: "red" }}>{pricePr}</Text> VNĐ/Kg</Text>
                                <Text style={styles.proDes}>{desProduct}</Text>
                                <View style={styles.buttonwrap}>
                                    <View style={styles.line1}>
                                        <Text style={{ color: '#000', marginTop: 5 }}>_________________</Text>
                                    </View>
                                    <TouchableHighlight
                                        style={styles.btn}
                                        onPress={this.onSubtract}
                                    >
                                        <Text style={styles.txt}> - </Text>
                                    </TouchableHighlight>
                                    <Text style={styles.proQuantity}>{this.state.quantity}</Text>
                                    <TouchableHighlight
                                        style={styles.btn}
                                        onPress={this.onAdd}
                                    >
                                        <Text style={styles.txt}> + </Text>
                                    </TouchableHighlight>
                                    <View style={styles.line2}>
                                        <Text style={{ color: '#000', marginTop: 5 }}>_________________</Text>
                                    </View>
                                </View>
                                <Text style={styles.total}>{i18n.t('trans_ProductDetailScreen.totalPrice')}: <Text style={{ color: "red" }}>{total}.00</Text> VNĐ </Text>
                            </View>

                        </View>
                        <View style={styles.proInfoWrap}>
                            <View style={styles.proInfo}>
                                <Text style={styles.proTitle2}>{i18n.t('trans_CommentScreen.title')}</Text>
                                <FlatList

                                    data={this.state.listCommentofProduct}
                                    renderItem={({ item }) =>
                                        <View style={{ flex: 1, flexDirection: 'row', marginTop: 5, backgroundColor: 'white', borderRadius: 60, borderRadius: 15, borderWidth: 3, }}>
                                            <View style={{ flex: 3, justifyContent: 'center', alignItems: 'center' }}>
                                                <Image style={{ borderRadius: 50, width: 60, height: 60, borderWidth: 1, }} source={{ uri: item.avataImage }}></Image>
                                            </View>
                                            <View style={{ flex: 7, justifyContent: 'center', marginLeft: 5, marginTop: 5 }}>
                                                <Text style={{ fontSize: 25, color: 'blue', fontFamily: 'Lobster-Regular', textDecorationLine: 'underline', }}>
                                                    {item.username}:
                                                </Text>
                                                <Text style={{ fontSize: 20, color: 'black', fontFamily: 'Lobster-Regular' }}>
                                                    {item.description}
                                                </Text>
                                            </View>

                                        </View>
                                    }
                                />
                            </View>
                        </View>
                    </ScrollView>
                    {this.state.showButton}
                    <Modal isVisible={this.state.visibleModal === 1}>
                        {this.renderModalContent()}
                    </Modal>
                </View>
                {this.state.loading == true && <Loading />}
            </View>

        );
    }
}
const styles = StyleSheet.create({
    bgImg: {
        width: '100%',
        height: '100%',
        opacity: 0.4,
    },
    bgImg2: {
        width: 300,
        height: 150,
        borderRadius: 10
    },
    proImgwrap: {

        justifyContent: 'center',
        alignItems: 'center'
    },
    contentwrap: {
        flex: 1,
        position: 'absolute',
        padding: 20,
    },
    buttonCart: {

        alignSelf: 'center',
        position: 'absolute',
        bottom: 2,
        marginBottom: 4,
    },
    linearGradient: {
        flex: 1,
        paddingLeft: 60,
        paddingRight: 60,
        paddingTop: 7,
        paddingBottom: 7,
        borderRadius: 30,
    }, btnText: {
        color: '#fff',
        textAlign: 'center',
        fontFamily: 'Lobster-Regular',
        fontSize: 20,
    },
    proImg: {
        width: 200,
        height: 200,
        borderRadius: 15,
        borderWidth: 8,
        borderColor: '#ffffff',
    },
    proInfoWrap: {
        flex: 1,
        backgroundColor: 'transparent',
        marginTop: 20,
        borderRadius: 15,
        marginLeft: 2,
        marginRight: 2,
        borderWidth: 8,
        borderColor: '#fff',
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        right: 0,
        backgroundColor: "lightblue",
        padding: 14,
        margin: 13,
        height: 50,
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)",
    },
    modalContent: {
        flexDirection: 'row',
        backgroundColor: "white",
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)",
    },
    bottomModal: {
        justifyContent: "flex-end",
        margin: 0,
    },
    scrollableModal: {
        height: 300,
    },
    scrollableModalContent1: {
        height: 200,
        backgroundColor: "orange",
        alignItems: "center",
        justifyContent: "center",
    },
    scrollableModalContent2: {
        height: 200,
        backgroundColor: "lightgreen",
        alignItems: "center",
        justifyContent: "center",
    },
    proInfo2: {
        width: 350,
        height: 100,
        borderRadius: 15,
        backgroundColor: 'transparent',
        borderWidth: 3,
        borderColor: '#000',
        padding: 5
    },
    proInfo: {
        width: 350,
        height: 270,
        borderRadius: 15,
        backgroundColor: 'transparent',
        borderWidth: 3,
        borderColor: '#000',
        padding: 5
    },
    proTitle: {
        fontSize: 40,
        fontFamily: "Lobster-Regular",
        color: '#000',
        textAlign: 'center'
    },
    proTitle2: {
        fontSize: 30,
        fontFamily: "Lobster-Regular",
        color: '#000',
        textAlign: 'center'
    },
    proPrice: {
        fontSize: 20,
        fontFamily: "Lobster-Regular",
        color: '#000',
        textAlign: 'right',
        paddingRight: 5,
    },
    proDes: {
        fontSize: 20,
        fontFamily: "Lobster-Regular",
        color: '#000',
        textAlign: 'left',
    },

    buttonwrap: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 30,
    },
    proQuantity: {
        fontSize: 35,
        fontFamily: "Lobster-Regular",
        color: '#000',
        marginTop: 0
    },
    total: {
        fontFamily: "Lobster-Regular",
        fontSize: 30,
        color: "#000",
        textAlign: "center"
    },
    btn: {
        width: 30,
        height: 30,
        backgroundColor: "#000",
        borderRadius: 100,
        marginTop: 5
    },
    txt: {
        color: '#fff',
        textAlign: "center",
        fontSize: 20
    },
    btnwrap: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 10,
    },
    addcartbtn: {
        position: "absolute",
        width: 250,
        height: 45,
        backgroundColor: "#FF6600",
        borderRadius: 100,
        marginTop: 5,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    addcarttxt: {
        fontFamily: "Lobster-Regular",
        fontSize: 30,
        color: "#fff",
        textAlign: "center"
    }



})

