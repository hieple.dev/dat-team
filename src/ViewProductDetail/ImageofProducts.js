import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import Gallery from 'react-native-image-gallery';

export default class DemoGallery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      images: [
      ],
      listImagesLink: [],
    };
    this.onChangeImage = this.onChangeImage.bind(this);
  }
  componentDidMount=() => {
    this.refreshDatafromServer();
  }
  refreshDatafromServer = async () => {
   let { images } = this.state;
   try {
      const { navigation } = this.props;
      const itemId = navigation.getParam('IdImage', 'NO-ID');
      const response = await fetch('http://datapplication.tryfcomet.com/api/pro_images/'+itemId)
      let json = await response.json();
      json = json.data.map(x =>({...x, source: {uri: x.linkImage}}))
      images = images.concat(json);
      } catch (error) {
        console.log(error)
      }
   this.setState({images})
  }
  static navigationOptions = {
    title: 'Danh Mục',
    headerStyle: {
      backgroundColor: '#3ee2bf',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: '400',
      marginLeft: 95,
      marginTop: 10,
      fontSize: 28,
      fontFamily: "Lobster-Regular",
    },
    headerRight: (
      <Image
        source={require('../../assets/icons/icon_other.png')}
        style={{ width: 30, height: 30, marginRight: 10, opacity: 10 }}
      />
    ),
  };
  onChangeImage(index) {
    this.setState({ index });
  }

  renderError() {
    return (
      <View style={{ flex: 1, backgroundColor: 'black', alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ color: 'white', fontSize: 15, fontStyle: 'italic' }}>This image cannot be displayed...</Text>
        <Text style={{ color: 'white', fontSize: 15, fontStyle: 'italic' }}>... but this is fine :)</Text>
      </View>
    );
  }
  get galleryCount() {
    const { index, images } = this.state;
    return (
      <View style={{ top: 0, height: 65, backgroundColor: 'rgba(0, 0, 0, 0.7)', width: '100%', position: 'absolute', justifyContent: 'center' }}>
        <Text style={{ textAlign: 'right', color: 'white', fontSize: 15, fontStyle: 'italic', paddingRight: '10%' }}>{index + 1} / {images.length}</Text>
      </View>
    );
  }
  render() {
    return (
      <View style={{ flex: 1 }} >
        <Gallery
          style={{ flex: 1, backgroundColor: '#696969' }}
          images={this.state.images}
          errorComponent={this.renderError}
          onPageSelected={this.onChangeImage}
          initialPage={0}
        />
        {this.galleryCount}
      </View>
    );
  }
}