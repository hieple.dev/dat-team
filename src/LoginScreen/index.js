import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  BackHandler,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard, Image,
  Alert,
} from 'react-native';
import i18n from '../../locales/i18n';
import { userLoginFromClient } from '../NetworkAPI/index';
import TimerMixin from 'react-timer-mixin';
import Loading from '../Loading/index';


export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      loading: false
    };
  }
  clickLogin() {
    if (this.state.password == "" || this.state.email == "") {
      Alert.alert(
        i18n.t('trans_LoginAlert.trans_LoginFormAlert.alertTitle'),
        i18n.t('trans_LoginAlert.trans_LoginFormAlert.emptyInput'),
        [{ text: "OK" }],
        { cancelable: false }
      );
    } else {
        this.setState({ loading: true });
        const checkAccount = {
          email: this.state.email,
          password: this.state.password
      };    
      userLoginFromClient(checkAccount).then((message) => {
        if (message=="Successfully") {
          TimerMixin.setTimeout(() => {
            this.setState({ loading: false });     
            this.props.navigation.navigate('Main');
          }, 500);
        } else {
          Alert.alert(
            i18n.t('trans_LoginAlert.trans_LoginFailed.alertTitle'),
            i18n.t('trans_LoginAlert.trans_LoginFailed.alertContent'),
            [{text: i18n.t('trans_LoginAlert.trans_LoginFailed.alertText'),}],
            { 
              cancelable: false
            }
          );
           this.setState({ loading: false });
        }
      }).catch();      
    }
  }
  componentDidMount () {
    BackHandler.addEventListener ('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount () {
    BackHandler.removeEventListener ('hardwareBackPress', this.handleBackPress);
  }
  handleBackPress = () => {
    Alert.alert (
      'Exit App',
      'Do you want to exit?',
      [
        {
          text: 'No',
          style: 'cancel',
        },
        {text: 'Yes', onPress: () => BackHandler.exitApp ()},
      ],
      {cancelable: false}
    );
    return true;
  };
  render() {
    const Divider = (props) => {
      return <View {...props}>
        <View style={styles.line}></View>
      </View>
    }
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View>
          <Image source={require('../../assets/img/background/background_login.png')} style={styles.backgroundImage} />
          <View style={styles.loginContainer}>
            <View style={styles.up}>
              <Image source={require('../../assets/icons/auth/avatar_header.png')} style={styles.logo}></Image>
            </View>
            <View style={styles.down}>
              <View style={styles.textInputContainer}>
                <Image source={require('../../assets/icons/auth/user_icon.png')} style={{ width: 25, height: 25, alignItems: 'center', justifyContent: 'center', marginTop: 8, }}></Image>
                <View style={styles.textInputView}>
                  <TextInput
                    style={styles.textInput}
                    textContentType='emailAddress'
                    keyboardType='email-address'
                    placeholder={i18n.t('trans_LoginScreen.emailLabel')}
                    placeholderTextColor="rgba(1,1,1,0.8)"
                    onChangeText={(email) => this.setState({ email })}
                    value={this.state.email}
                  >
                  </TextInput>
                </View>
              </View>
              <View style={styles.textInputContainer}>
                <Image source={require('../../assets/icons/auth/lock_icon_pw.png')} style={{ width: 25, height: 25, marginTop: 8, }}></Image>
                <View style={styles.textInputView}>
                  <TextInput
                    style={styles.textInput}
                    secureTextEntry={true}
                    placeholder={i18n.t('trans_LoginScreen.passwordLabel')}
                    placeholderTextColor="rgba(1,1,1,0.8)"
                    onChangeText={password => this.setState({ password })}
                    value={this.state.password}>
                  </TextInput>
                </View>
              </View>
              <TouchableOpacity style={styles.loginButton} onPress={() => this.clickLogin()}>
                <Text style={styles.loginButtonTitle}>{i18n.t('trans_LoginScreen.loginBtn')}</Text>
              </TouchableOpacity>
              <Divider style={styles.divider}></Divider>
              <View style={{ flexDirection: 'row', marginTop: 10, }}>
                <Text style={styles.title1}>{i18n.t('trans_ForgotPasswordLink.forgotPassword')}</Text>
                <Text style={styles.title2} onPress={() => this.props.navigation.navigate('Register')}>{i18n.t('trans_LoginScreen.registerAccount')}</Text>
              </View>
            </View>
          </View>
          {this.state.loading == true && <Loading />}
        </View>
      </TouchableWithoutFeedback>


    );
  }
}
const styles = StyleSheet.create({
  title1: {
    fontSize: 16,
    color: 'white',
    fontFamily: "Lobster-Regular",
  },
  title2: {
    marginLeft: 90,
    fontSize: 16,
    color: 'white',
    fontFamily: "Lobster-Regular",
  },
  backgroundImage: {
    height: '100%',
    width: '100%'
  },
  loginContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  textInput: {
    marginLeft: 10,
    fontFamily: "Lobster-Regular",
  },
  up: {
    flex: 4.5,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  down: {
    flex: 7,//70% of column
    marginTop: 20,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  title: {
    color: 'white',
    color: 'red',
    textAlign: 'center',
    width: 400,
    fontSize: 23
  },
  textInputContainer: {
    paddingHorizontal: 10,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    borderRadius: 6,
    marginBottom: 20,

  },
  textInputView: {
    marginLeft: 10,
    marginRight: -10,
    justifyContent: 'center',
    alignItems: 'flex-start',
    width: 300,
    height: 42,
    borderRadius: 5,
    backgroundColor: 'rgba(255,255,255,0.7)',

  },
  loginButton: {
    width: 320,
    height: 45,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#48dab7',


  },
  loginButtonTitle: {
    fontSize: 18,
    color: 'white',
    fontFamily: "Lobster-Regular",

  },
  facebookButton: {
    width: 300,
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
  },
  line: {
    height: 1.5,
    marginTop: 20,
    flex: 2,
    backgroundColor: 'white'
  },
  textOR: {
    flex: 1,
    textAlign: 'center'
  },
  divider: {
    flexDirection: 'row',
    height: 40,
    width: 298,
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    marginTop: 40,
    width: 310,
    height: 135,
  }
})
