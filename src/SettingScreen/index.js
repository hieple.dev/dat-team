import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, TouchableHighlight,AsyncStorage,Alert,BackHandler } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
export default class example2 extends Component {
    static navigationOptions = {
        headerLeft: (
            <Image
                source={require('../../assets/img/logo/logo_icon.png')}
                style={{ width: 120, height: 40, marginLeft: 10 }}
            />
        ),
        title: 'Profile',
        headerStyle: {
            backgroundColor: '#3ee2bf',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: '400',
            marginLeft: 95,
            marginTop: 10,
            fontSize: 28,
            fontFamily: "Lobster-Regular",
        },
    };
    constructor(props) {
        super(props);
        this.state = {
            cartItems: [],
            userUsingApp: ''
        };
    }
    componentWillMount() {
       this._checkUserUsingApp();
      }
      _checkUserUsingApp = async () => {   
        try {     
             const usernameUsingApp = await AsyncStorage.getItem('usernameUsingApp'); 
             this.setState({
               userUsingApp: usernameUsingApp
             })
            } catch (error) {   
          // Error retrieving data  
        } 
      };
      async logoutApp() {
        try {
          
          await AsyncStorage.removeItem('emailUsingApp');
          await AsyncStorage.removeItem('passwordUsingApp');
          await AsyncStorage.removeItem('idUserUsingApp');
          await AsyncStorage.removeItem('usernameUsingApp');
          await AsyncStorage.removeItem('avataImageUsingApp');
          await AsyncStorage.removeItem('CART');
          BackHandler.exitApp ()
        } catch (error) {
          console.log("Error resetting data" + error);
        }
      }
    render() {
        return (
            <View style={{ flex: 1, alignItems: "center" }}>
                  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
              <Text style={{
                fontSize: 20,
                color: 'black',
                fontFamily: "Lobster-Regular",
              }}
              >Hello <Text style={{color:'red'}}>{this.state.userUsingApp}</Text></Text>
              {this.state.loading == true && <Loading />}
            </View>
                    <View style={styles.buttonCart}>
                        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#ff8100', '#ff4300', '#ff1100']} style={styles.linearGradient}>
                            <TouchableHighlight
                          onPress={() => 
                            Alert.alert(
                              'Warning !!!',
                              'Do you want to logout the application',
                              [
                                {
                                  text: 'Ok',
                                  onPress: () =>  this.logoutApp(),
                                  style: 'cancel',
                                },
                                {text: 'Cancel', onPress: () =>   this.props.navigation.navigate('Main')},
                              ],
                              {cancelable: false},
                            )
                           
                            }
                            >
                                <Text style={[styles.btnText]}>Logout</Text>
                            </TouchableHighlight>
                        </LinearGradient>
                    </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    backgroundImg: {
        width: '100%',
        height: '100%',
        opacity: 0.2,
    },

    btnText: {
        color:'#fff',
        textAlign:'center',
        fontFamily: 'Lobster-Regular',
        fontSize: 20,
    },

    linearGradient: {
        flex: 1,
        paddingLeft: 60,
        paddingRight: 60,
        paddingTop: 7,
        paddingBottom: 7,
        borderRadius: 30,
    },

    contentWrap: {
        marginTop:20,
        flex: 1,
        position: "absolute",  
    },

    buttonCart: {
        alignSelf: 'center',
        position: 'absolute',
        bottom: 0,
      
    },

    formWrap: {
        width: 400,
        height: 480,
        borderRadius: 15,
        backgroundColor: 'transparent',
        borderWidth: 3,
        borderColor: '#000',
        marginTop: 0,
    },

    titles: {
        flexDirection: 'row',
        height: 50,
        borderWidth: 1,
        borderColor: "#000",
        borderRadius: 10,
        marginTop: 10,
        marginLeft: 5,
        marginRight: 5,
    },

    title: {
        flex: 3,
        fontFamily: "Lobster-Regular",
        fontSize: 18,
        color: "#000",
        marginLeft: 10,
        marginTop: 10,
    },

    myinput: {
        flex: 7,
        fontFamily: "Lobster-Regular",
        fontSize: 18,
        color: "#000",
        marginLeft: 10,
    },

    mytext: {
        fontFamily: "Lobster-Regular",
        fontSize: 16,
        color: "#000",
        marginLeft: 10,
    },

    btnwrap: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },

    confirmbtn: {
        position: "absolute",
        width: 350,
        height: 50,
        backgroundColor: "#FF6600",
        borderRadius: 100,
        marginTop: 5,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    confirmtxt: {
        fontFamily: "Lobster-Regular",
        fontSize: 25,
        color: "#fff",
        textAlign: "center"
    }

})  
