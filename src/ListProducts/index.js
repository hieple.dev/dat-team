import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, Text, Image } from 'react-native';
import Loading from '../Loading/index';
import { FlatGrid } from 'react-native-super-grid';
import { getProductFromCategory } from '../NetworkAPI/index';
import i18n from '../../locales/i18n';
export default class ListProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      listProductFromCategory: []
    };
  }
  static navigationOptions = {
    title: i18n.t('trans_HeaderSection.headerTitleProductList'),
    headerStyle: {
      backgroundColor: '#3ee2bf',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: '400',
      marginLeft: 75,
      marginTop: 10,
      fontSize: 28,
      fontFamily: "Lobster-Regular",
    },
  };
  componentDidMount() {
    this.refreshDatafromServer();
  }
  refreshDatafromServer = () => {
    const { navigation } = this.props;
    const itemId = navigation.getParam('itemId', 'NO-ID');
    getProductFromCategory(itemId).then((data) => {
      this.setState({
        loading: false,
        listProductFromCategory: data
      })
    }).catch((error) => {
      this.setState({
        loading: false,
        listProductFromCategory: []
      })
    })
  }
  showProduct(){
    if (this.state.listProductFromCategory.length<=0) {
     return( <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',}}>             
              <Text style={{fontSize: 20,
              color: 'black',
              fontFamily: "Lobster-Regular",}}
              >Your wishlist is empty...</Text>
            </View>);
    } else {
      return(
        <FlatGrid
              itemDimension={240}
              items={this.state.listProductFromCategory}
              style={styles.gridView}
              renderItem={({ item, index }) => (
                <View style={[styles.itemContainer, { backgroundColor: 'white' }]}>
                  <View style={{ flex: 1, borderRadius: 10, flexDirection: 'row' }}>
                    <TouchableOpacity onPress={() => {
                      this.props.navigation.navigate('ViewProductDetail', {
                        itemIdProduct: item.id,
                        nameProduct: item.name,
                        priceProduct: item.unitPrice,
                        desProduct: item.description,
                        linkImageProduct: item.linkImage
                      })
                    }}>
                      <View>
                        <Image source={{ uri: item.linkImage }} style={{ width: 130, height: 120, borderRadius: 10, }}></Image>
                      </View>
                    </TouchableOpacity>
                    <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'flex-start', marginLeft: 10, borderRadius: 10, borderWidth: 2 }}>
                      <View style={{ marginLeft: 10 }}>
                        <Text style={styles.itemName} numberOfLines={1} ellipsizeMode={'tail'}><Text>{item.name}</Text></Text>
                        <Text style={styles.itemCode}>{i18n.t('trans_ProductListScreen.salesman')}: <Text style={{ color: 'blue' }}>{item.fullname}</Text></Text>
                        <Text style={styles.itemCode} numberOfLines={1} ellipsizeMode={'tail'}>{i18n.t('trans_ProductListScreen.description')}: <Text style={{ color: 'red' }} >{item.description}</Text></Text>
                        <Text style={styles.itemCode2}>{i18n.t('trans_ProductListScreen.price')}: <Text style={{ color: '#0eb70a' }}>{item.unitPrice}</Text> VND/kg</Text>
                      </View>
                    </View>
                  </View>
                </View>
              )}
            />
      );
    }
   
  }
  render() {
    return (
      <View>
        <Image source={require('../../assets/img/background/background_wcyan_home.png')} style={styles.backgroundImage} />
        <View style={styles.loginContainer}>
          {this.showProduct()}
          {this.state.loading == true && <Loading />}
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    height: '100%',
    width: '100%',
    opacity: 0.75
  },
  loginContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    flex: 1,
  },
  gridView: {
    flex: 1,
  },
  itemContainer: {
    flexDirection: 'row',
    borderWidth: 5,
    borderColor: '#e67e22',
    borderRadius: 10,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 30,
    color: 'black',
    width: 200,
    fontFamily: "Lobster-Regular",
  },
  itemCode: {
    marginLeft: 10,
    fontSize: 16,
    fontWeight: '100',
    color: 'black',
    fontFamily: "Lobster-Regular",
    width: 200
  },
  itemCode2: {
    textAlign: 'right',
    fontSize: 16,
    fontWeight: '100',
    color: 'black',
    fontFamily: "Lobster-Regular",
    width: 200
  },
});