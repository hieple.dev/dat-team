import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableHighlight, AsyncStorage } from 'react-native';
import { SectionGrid } from 'react-native-super-grid';
import i18n from '../../locales/i18n';
import Loading from '../Loading/index';
import { getFinishOrder, getUnFinishOrder } from '../NetworkAPI/index';
import LinearGradient from 'react-native-linear-gradient';

export default class History extends Component {
  static navigationOptions = {
    headerLeft: (
      <Image
        source={require('../../assets/img/logo/logo_icon.png')}
        style={{ width: 125, height: 45, marginLeft: 10 }}
      />
    ),
    title: i18n.t('trans_HeaderSection.headerTitleHistory'),
    headerStyle: {
      backgroundColor: '#3ee2bf',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: '400',
      marginLeft: 95,
      marginTop: 10,
      fontSize: 28,
      fontFamily: "Lobster-Regular",
    },
  };
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      listOrderFinish: [],
      listOrderUnFinish: [],
      idUserCheck: ''
    };
  }

  componentDidMount() {
    this._checkUserUsingApp()
  }
  _checkUserUsingApp = async () => {
    try {
      this.setState({
        idUserCheck: await AsyncStorage.getItem('idUserUsingApp')
      })
      this.refreshFinishfromServer();
      this.refreshUnFinishfromServer();
    } catch (error) {

    }
  };
  refreshFinishfromServer = () => {
    const itemId = this.state.idUserCheck;
    getFinishOrder(itemId).then((data) => {
      this.setState({
        loading: false,
        listOrderFinish: data
      })
    }).catch((error) => {
      this.setState({
        loading: false,
        listOrderFinish: []
      })
    })
  }

  refreshUnFinishfromServer = () => {
    const itemId = this.state.idUserCheck;
    getUnFinishOrder(itemId).then((data) => {
      this.setState({
        loading: false,
        listOrderUnFinish: data
      })
    }).catch((error) => {
      this.setState({
        loading: false,
        listOrderUnFinish: []
      })
    })
  }
  _statusProduct(statusPr) {
    if (statusPr == '0') {
      return (
        <Text style={styles.inProgress}>{i18n.t('trans_PurchaseHistoryScreen.haveNotReceiveYet')}</Text>
      )

    } else {
      return (
        <Text style={styles.doneShipping}>{i18n.t('trans_PurchaseHistoryScreen.received')}</Text>
      )

    }
  }
  _resetList() {
    this.refreshFinishfromServer();
    this.refreshUnFinishfromServer();
  }
  render() {
    if ((this.state.listOrderFinish.length <= 0) && (this.state.listOrderUnFinish.length <= 0)) {
      return (

        <View style={{ flex: 1, backgroundColor: '#ffffff', flexDirection: 'column' }}>
          <View style={styles.listProduct}>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
              <Text style={{
                fontSize: 20,
                color: 'black',
                fontFamily: "Lobster-Regular",
              }}
              >Your order history is empty...</Text>
              {this.state.loading == true && <Loading />}
            </View>
          </View>
          <View style={{ width: '100%', height: 40, justifyContent: 'center', flexDirection: 'row' }}>
            <View style={styles.buttonCart}>
              <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#ff8100', '#ff4300', '#ff1100']} style={styles.linearGradient}>
                <TouchableHighlight

                  onPress={() => this._resetList()}
                >
                  <Text style={[styles.btnText]}> {i18n.t('trans_CartScreen.refreshCartBtn')}</Text>
                </TouchableHighlight>
              </LinearGradient>
            </View>
          </View>
          {this.state.loading == true && <Loading />}
        </View>
      );
    } else {
      return (
        <View style={{ flex: 1, backgroundColor: '#ffffff', flexDirection: 'column' }}>
          <View style={styles.listProduct}>
            <SectionGrid
              itemDimension={200}
              sections={[
                {
                  title: i18n.t('trans_PurchaseHistoryScreen.haveNotReceiveYet'),
                  data: this.state.listOrderFinish.slice(0, this.state.listOrderFinish.length),
                },
                {
                  title: i18n.t('trans_PurchaseHistoryScreen.received'),
                  data: this.state.listOrderUnFinish.slice(0, this.state.listOrderUnFinish.length),
                },
              ]}
              style={styles.gridView}
              renderItem={({ item, section, index }) => (
                <View style={styles.itemContainer}>
                  <View style={{ flex: 3, }}>
                    <Image source={{ uri: item.linkImage }} style={{ width: 80, height: 80, borderRadius: 5, }} />
                  </View>
                  <View style={{ flex: 7, }}>
                    <Text style={styles.productName}>{item.name} ( {item.quantity}kg ) </Text>
                    <Text style={styles.productInfor}><Text>{i18n.t('trans_PurchaseHistoryScreen.bill')}:  </Text><Text style={styles.productPrice}>{item.unitPrice * item.quantity + 20}</Text><Text> VNĐ</Text></Text>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.productInfor}>{i18n.t('trans_PurchaseHistoryScreen.status')}:  </Text>
                      {this._statusProduct(item.status)}

                    </View>
                  </View>
                </View>
              )}
              renderSectionHeader={({ section }) => (
                <Text style={styles.sectionHeader}>{section.title}</Text>
              )}
            />
          </View>
          <View style={{ width: '100%', height: 40, justifyContent: 'center', flexDirection: 'row' }}>
            <View style={styles.buttonCart}>
              <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#ff8100', '#ff4300', '#ff1100']} style={styles.linearGradient}>
                <TouchableHighlight

                  onPress={() => this._resetList()}
                >
                  <Text style={[styles.btnText]}> {i18n.t('trans_CartScreen.refreshCartBtn')}</Text>
                </TouchableHighlight>
              </LinearGradient>
            </View>
          </View>
          {this.state.loading == true && <Loading />}
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  listProduct: {
    flex: 1,
    borderColor: '#000000',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderRadius: 5,
    marginTop: 10,
    marginRight: 10,
    marginLeft: 10,
    marginBottom: 10,
  },
  gridView: {
    flex: 1,
  },
  buttonCart: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: 2,
  },
  btnText: {
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'Lobster-Regular',
    fontSize: 20,
  },

  btnCart: {
    width: 200,
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#c96c6e',
    borderRadius: 30,
    borderWidth: 1,
    borderColor: '#fff',
    justifyContent: "flex-end",
    marginLeft: 7.5,
    marginRight: 15,
  },

  linearGradient: {
    flex: 1,
    paddingLeft: 60,
    paddingRight: 60,
    paddingTop: 7,
    paddingBottom: 7,
    borderRadius: 30,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    borderColor: '#3ee2bf',
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderLeftWidth: 3,
    borderRightWidth: 3,
    borderRadius: 5,
    padding: 7,
    height: 100,
  },
  productName: {
    fontFamily: 'Lobster-Regular',
    color: 'black',
    fontSize: 20,
  },
  productInfor: {
    fontFamily: 'Lobster-Regular',
    color: 'black',
    fontSize: 16,
  },
  productPrice: {
    color: 'red',
  },
  sectionHeader: {
    flex: 1,
    fontFamily: 'Lobster-Regular',
    fontSize: 16,
    alignItems: 'center',
    backgroundColor: '#ffffff',
    color: 'black',
    paddingTop: 10,
    marginLeft: 10,
    marginRight: 10,
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  inProgress: {
    fontFamily: 'Lobster-Regular',
    color: 'red',
    fontSize: 16,
  },
  doneShipping: {
    fontFamily: 'Lobster-Regular',
    color: 'green',
    fontSize: 16,
  },
});