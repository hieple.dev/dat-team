import {AsyncStorage} from 'react-native'
  
const ipAPIforApp = 'http://datapplication.tryfcomet.com'
const apiGetCategory = ipAPIforApp + '/api/category'
const apiGetProduct = ipAPIforApp + '/api/cate_product/'
const getCommentProduct = ipAPIforApp + '/api/comment_product/'
const getQuantityofProduct = ipAPIforApp + '/api/pro_quantity/' //edit
const apiAddNewAccount = ipAPIforApp + '/api/register'
const apiCheckUser = ipAPIforApp + '/api/checkLogin'
const apiGetImages = ipAPIforApp + '/api/pro_images/'
const apiCommentforProduct = ipAPIforApp + '/api/sendcomment'
const apiCheckoutforProduct = ipAPIforApp + '/api/checkout'
const apiGetFinishOrder = ipAPIforApp + '/api/finishOrder/'
const apiGetUnFinishOrder = ipAPIforApp + '/api/unFinishOrder/'
async function getCategoryFromServer() {
    try {
        let response = await fetch(apiGetCategory);
        let responseJson = await response.json();
        return responseJson.data;
    } catch (error) {
        console.error(`Error is: ${error}`);
    }
}
async function getImageofProducts(id) {
    try {
        let response = await fetch(apiGetImages+id);
        let responseJson = await response.json();
        return responseJson.data;
    } catch (error) {
        console.error(`Error is: ${error}`);
    }
}
async function getProductFromCategory(id) {
    try {
        let response = await fetch(apiGetProduct+id);
        let responseJson = await response.json();
        return responseJson.data;
    } catch (error) {
        console.error(`Error is: ${error}`);
    }
}
async function getCommentProductDetail(id) {
    try {
        let response = await fetch(getCommentProduct+id);
        let responseJson = await response.json();
        return responseJson.data;
    } catch (error) {
        console.error(`Error is: ${error}`);
    }
}
async function getQuantityofProductinCategory(id) {
    try {
        let response = await fetch(getQuantityofProduct+id);
        let responseJson = await response.json();
        return responseJson.data;
    } catch (error) {
        console.error(`Error is: ${error}`);
    }
}
async function insertNewAccountToServer(params) {
    try {
        let response = await fetch(apiAddNewAccount,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        });
        let responseJson = await response.json();
        return responseJson.message;
    } catch (error) {
        console.error(`Error is: ${error}`);
    }
}
async function userLoginFromClient(params) {
    try {
        
        let response = await fetch(apiCheckUser,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        });
        let responseJson = await response.json();
        let idUser = 0;
        let username = null;
        let avatarImage = '';
        if (responseJson.message=="Successfully") {
            await AsyncStorage.setItem('emailUsingApp', params.email);
            await AsyncStorage.setItem('passwordUsingApp', params.password);
             responseJson.userinfo.map((data)=>{
              idUser = data.id;
              username = data.username;
              avatarImage =  data.avataImage;
        })
            await AsyncStorage.setItem('idUserUsingApp', idUser);
            await AsyncStorage.setItem('usernameUsingApp', username);
            await AsyncStorage.setItem('avataImageUsingApp', avatarImage);  
        }
        return responseJson.message;
    } catch (error) {
        console.error(`Error is: ${error}`);
    }
}

async function commentforProduct(params) {
    try {
        
        let response = await fetch(apiCommentforProduct,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        });
        let responseJson = await response.json();
        return responseJson.message;
    } catch (error) {
        console.error(`Error is: ${error}`);
    }
}

async function checkoutforProduct(params) {
    try {
        
        let response = await fetch(apiCheckoutforProduct,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        });
        let responseJson = await response.json();
        return responseJson.message;
    } catch (error) {
        console.error(`Error is: ${error}`);
    }
}

async function getUserInfoFromServer(params) {
    try {
        let response = await fetch(apiCheckUser,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        });
        let responseJson = await response.json();
        console.log(responseJson);
        return responseJson.userinfo;
    } catch (error) {
        console.error(`Error is: ${error}`);
    }
}
async function getFinishOrder(id) {
    try {
        let response = await fetch(apiGetFinishOrder+id);
        let responseJson = await response.json();
        return responseJson.data;
    } catch (error) {
        console.error(`Error is: ${error}`);
    }
}
async function getUnFinishOrder(id) {
    try {
        let response = await fetch(apiGetUnFinishOrder+id);
        let responseJson = await response.json();
        return responseJson.data;
    } catch (error) {
        console.error(`Error is: ${error}`);
    }
}
export {getFinishOrder};
export {getUnFinishOrder};
export {getCategoryFromServer};
export {getProductFromCategory};
export {getQuantityofProductinCategory};
export {getCommentProductDetail};
export {insertNewAccountToServer};
export {userLoginFromClient};
export {getImageofProducts};
export {commentforProduct};
export {checkoutforProduct};
export {getUserInfoFromServer};